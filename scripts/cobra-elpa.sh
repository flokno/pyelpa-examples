#!/bin/bash -l
# Standard output and error:
#SBATCH -o ./tjob.out.%j
#SBATCH -e ./tjob.err.%j
# Initial working directory:
#SBATCH -D ./
# Job Name:
#SBATCH -J test
# Queue (Partition):
#SBATCH --partition=short
# Number of nodes and MPI tasks per node:
#SBATCH --nodes=2
#SBATCH --ntasks-per-node=40 
#
#SBATCH --mail-type=BEGIN
#SBATCH --mail-user=litmann@fhi-berlin.mpg.de
#
# Wall clock limit:
#SBATCH --time=00:05:00


export OMP_NUM_THREADS=1

module purge
module load intel impi anaconda mpi4py  mkl

INSTALL_DIR=/u/lyair/codes/ELPA/2020/elpa_with_python
export PYTHONPATH=$PYTHONPATH:$INSTALL_DIR/lib/python3.7/site-packages/

srun -n 80  python -u diag.py >out 
