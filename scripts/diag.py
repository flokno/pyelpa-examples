import sys
import time

import h5py
import numpy as np

from mpi4py import MPI
from pyelpa import DistributedMatrix, Elpa, ProcessorLayout

np.set_printoptions(threshold=sys.maxsize, linewidth=10000, precision=3)


# Open hdf5 file
name = "dynmat.hdf5"
f = h5py.File(name, "r")
first_key = list(f.keys())[0]
matrix = f[first_key]

# Set some parameters for matrix layout
na = matrix.shape[0]
nev = na
nblk = 64


# use a diagonal matrix as input
a = DistributedMatrix.from_comm_world(na, nev, nblk)
comm = MPI.COMM_WORLD

# SET Matrix OPtion 1
# a.set_data_from_global_matrix(matrix)

# SET Matrix OPtion 2
rows, columns = a.get_global_index(np.arange(a.na_rows), np.arange(a.na_cols))

column_blocks = np.split(columns, a.na_cols // nblk)
row_blocks = np.split(rows, a.na_rows // nblk)
nblocks = len(row_blocks) * len(column_blocks)

if comm.Get_rank() == 0:
    time0 = time.time()

if na % nblk == 0 and nblocks < a.na_rows:
    if comm.Get_rank() == 0:
        print(
            "Set up matrix by blocks {} ".format(nblocks)
            + "instead of by rows {}\n".format(a.na_rows)
        )
    ncount = 0
    for i, r in enumerate(row_blocks):
        for j, c in enumerate(column_blocks):
            ncount += 1
            if comm.Get_rank() == 0 and ncount % 50 == 0:
                print("{} of {} blocks".format(ncount, nblocks))
            a.data[i * nblk : (i + 1) * nblk, j * nblk : (j + 1) * nblk] = matrix[
                r[0] : r[-1] + 1, c[0] : c[-1] + 1
            ]
else:
    if comm.Get_rank() == 0:
        print(
            "Set up matrix by rows {} instead of by blocks {}\n".format(
                a.na_rows, nblocks
            )
        )
    for i, r in enumerate(rows):
        a.data[i] = matrix[r, columns]

if comm.Get_rank() == 0:
    time1 = time.time()
    print("Reading time {} s".format(time1 - time0))
    print("Init diagonalization")
# DIAG

data = a.compute_eigenvectors()
eigenvalues = data["eigenvalues"]
eigenvectors = data["eigenvectors"]

if comm.Get_rank() == 0:
    time2 = time.time()
    print("Diagonalization time {} s".format(time2 - time1))

#####################    Save        #####################################
# print("Rank: {} has {} rows and  {} columns.".format(comm.Get_rank(),a.na_rows, a.na_cols))
if comm.Get_rank() == 0:
    print("Done ...")
    np.savetxt("eigval.dat", eigenvalues)

    time3 = time.time()
    print("Total time {} s".format(time3 - time0))
